let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print(){
	return collection;
}

function enqueue(name){

	collection[collection.length] = name

	return collection;

}

function dequeue(){
	let nameCollection = [];
	for (let i = 0; i < collection.length; i++) {
		if (i > 0){
			nameCollection[i-1] = collection[i];
		}
	}
	collection = nameCollection;
	return collection;
}



function front(){
	return collection[0];
}



function size(){
	return collection.length;
}



function isEmpty(){
	return collection.length === 0;
}





module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};